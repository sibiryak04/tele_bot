from telethon import TelegramClient, sync, events
from colorama import Fore, Style, init
import re
import telebot
import time

k = 1							#Минимальный пробив линии в секундах

importChannel = 'stata_mk'		#Название канала откуда берется статистика
exportChannel = 'Signals_MKX'	#Куда будут отправляться сообщения
testChannel = 'testchanle04'	#Тестовый канал

filemsg = 'lastmsg.txt'			#файл с последними сообщениями
init()
#конфиги
bot = telebot.TeleBot("875495523:AAF1hvp9uNLcgy83Dp1MMRI31lMywx6hoMw")#API ключ для телеграмм бота
# Вставляем api_id и api_hash это для telethon api приложения
api_id = 1478146
api_hash = '3fa84a9101a46fbdf33e8540b7c6194b'

class MsgRe:#обявление класса для присвоения значений из сообщения
	teleid = None			#ID сообщения телеграмм
	time = None				#Время взятое из сообщения
	players = None			#Игроки
	qP = None				#Коэффициенты на победу
	qF = None				#Коэффициенты на фаталити\бруталити\без добивания
	qT = None				#Коэффициенты на тотал
	resinRound = []			#Результаты по каждому раунду победивший\вид финала(F;B;R)\тотал
	resultAll = None		#статистика побед за бой

def send(time,players,qP,qF,qT,resinRound=None,resultAll=None,teleid=None):
	text = time[0] + '\nКопипаст статистики с группы\n' + players[0] + ' - ' + players[1] + '\n' + players[2] + '\nКоэффициенты на победу П1/П2: \n' + qP[0][0] + '/' + qP[0][1] + '\nКоэффициенты на Фатилити/Бруталити/Без добивания: \nF ' + qF[0][0] +'/B ' + qF[0][1] + '/R ' + qF[0][2] + '\nКоэффициенты на тотал: \n' + qT[0] + '/' + qT[1] + '/' + qT[2] +'\n\n'
	if resinRound:
		for i in range(len(resinRound)):
			text = text+str(i+1)+'. '+resinRound[i][0][0]+'-'+resinRound[i][0][1]+'-'+resinRound[i][0][2]+'\n'
	else:
		#print('Переменная не пришла условие не сработало')
		pass

	if resultAll:
		text = text+'\n'+resultAll[0]+'\n'
	else:
		pass
	#print(text)
	return text

def convertListToStrForWrite(tempmsg):#Принимает массив с данными и конвертирует в строку для записи в файл
	txt=''
	for i in range(len(tempmsg)):
		txt = txt+str(tempmsg[i][0])+'-'+(tempmsg[i][1])+'\n'
	return txt
def get_info_fromtxt(line):
	result = re.findall(r'\d+-[0-9 :.]*',line)
	result = re.split(r'-',line)
	result[1] = result[1].rstrip('\n')
	return result
def getIdOfMsg(temp,time):
	for i in range(len(temp)):
		if temp[i][1] == time[0]:
			return temp[i][0]
		else:
			pass
def information(color,text):
	dateTime = time.strftime("%d.%m.%y %H:%M:%S", time.localtime())
	if color == 'g':
		print('[' + Fore.GREEN + 'ИНФО' + Fore.WHITE + '] '+dateTime+' '+str(text))
	elif color =='y':
		print('[' + Fore.YELLOW + 'ИНФО' + Fore.WHITE + '] '+dateTime+' '+str(text))
	elif color == 'r':
		print('[' + Fore.RED + 'ИНФО' + Fore.WHITE + '] '+dateTime+' '+str(text))
	else:
		print('Неверно переданы вргументы сообщения')
	pass

def centerLineCheck(q,r):
	#print(len(r))
	if len(r) is int(1):#Проверяем сколько прошло раундов условие срабатывает если раунд только один rR[2]+k < qT[0] or rR[2]-k > qT[0]
		if float(r[0][0][2])+k < float(q[0]) or float(r[0][0][2])-k > float(q[2]):
			#print('пробив есть')
			return True
		else:
			#print('пробив нет')
			return False
	elif len(r) is int(2):#условие срабатывает если в сообщении 2 раунда
		if float(r[0][0][2])+k < float(q[0]) and float(r[1][0][2])+k < float(q[0]):
			#print('Подтверждаем сигнал, тотал больше'+q[1])
			return True
		elif float(r[0][0][2])-k > float(q[2]) and float(r[1][0][2])-k > float(q[2]):
			#print('Подтверждаем сигнал, тотал меньше'+q[1])
			return True
		else:
			#print('Сигнал не подтвержден')
			return False
	else:
		pass

def readTxt():#Возвращает массив из файла формата [['217','12.08.2020 00:20:00'],['217','12.08.2020 00:20:00']]
	lastmsg = open(filemsg,'r')
	tempmsg = []
	for line in lastmsg:
		tempmsg.append(get_info_fromtxt(line))#добавление строки к массиву с последними сообщениями парсинг txt файла в массив
	lastmsg.close()
	return tempmsg

def appendToTxt(MsgId,time):#Функция проверяет кол-во записей в файле, удаляет если больше 10 и записывает в конец файла переданную строку в нужном формате
	tempmsg=readTxt()
	if len(tempmsg)>=10:#считываем количество записей в файле, если больше десяти то удаляем пока не останется 9
		for count in range(len(tempmsg)):
			tempmsg.remove(tempmsg[0])
			if len(tempmsg)>=10:
				continue
			else:
				break
	newstr = [MsgId,time[0]]
	tempmsg.append(newstr)
	lastmsg = open(filemsg,'w')
	lastmsg.write(convertListToStrForWrite(tempmsg))
	lastmsg.close()

def delStrTxt(time):
	lastmsg = open(filemsg,'r')
	tempmsg = []
	for line in lastmsg:
		str1 = get_info_fromtxt(line)
		if str1[1] != time[0]:
			tempmsg.append(get_info_fromtxt(line))#добавление строки к массиву с последними сообщениями парсинг txt файла в массив
		else: pass
	lastmsg.close()
	lastmsg = open(filemsg,'w')
	lastmsg.write(convertListToStrForWrite(tempmsg))
	lastmsg.close()

def returnIdMsgTxt(time):
	tempmsg=readTxt()
	for i in range(len(tempmsg)):
		if time[0] == tempmsg[i][1]:
			idMsg = tempmsg[i][0]
			#information('g','Сообщение найдено в списке')
			tempmsg = []
			return idMsg