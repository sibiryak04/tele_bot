#-----------------------------------------------------
# ЗАДАЧА СДЕЛАТЬ ЧТОБЫ БОТ ОТПРАВЛЯЛ И РЕДАКТИРОВАЛ СООБЩЕНИЯ В ОСНОВНОЙ ЧАТ
#-----------------------------------------------------
from config import *



client = TelegramClient('session_name', api_id, api_hash)
client.start()
information('g','Бот успешно запущен!!!')
@client.on(events.MessageEdited(chats=(importChannel)))
async def handler(event):
	MsgRe.resinRound = []
	# Log the date of new edits
	result = re.split(r'[\n]', event.message.to_dict()['message'])
	if re.search(r'\d+.\d+.\d+. \d+:\d+:\d+',result[0]):#проверили что было отредактировано не рекламное сообщение
		#print('Поймано событие "отредактировано сообщение со статистикой"')
		
		information('y','В канале-источнике было отредактировано сообщение')
		MsgRe.players = re.findall(r'#(\w+)',str([result[1], result[2]]))	#Получаем имена игроков без хэштега и заносим в массив ['Кенши','КунгЛао','Кенши_КунгЛао']
		MsgRe.time = re.findall(r'\d+.\d+.\d+. \d+:\d+:\d+',result[0])		#Получаем время получения статистики день.месяц.год часы:минуты:секунды
		MsgRe.qP = re.findall(r'П1/П2 - ([0-9.]*)/([0-9.]*)',result[3])							#коэфициенты на победу игроков П1/П2
		MsgRe.qF = re.findall(r'F ([0-9.]*)/B ([0-9.]*)/R ([0-9.]*)',result[4])					#возвращает массив с коэфициентами на F/B/R
		MsgRe.qT = re.split(r'/',result[5])
		for val in result:											#пробегает по строкам сообщения и ищет данные о раундах
			if re.match(r'\d+. \w\d-\w-\d+',val):
				#print('Обнаружены данные о раунде')
				#print(re.findall(r'\d+. (\w\d)-(\w)-(\d+)',val))
				MsgRe.resinRound.append(re.findall(r'\d+. (\w\d)-(\w)-(\d+)',val)) #Возвражает трехмерный массив с данными по каждому раунду [[(П1-R-32)],[(П2-F-28)],....]
			else:
				#print('Данных о раунде не обнаружено')
				pass
		for val in result:											#пробегает по строкам сообщения и ищет данные о раундах
			if re.match(r'\d:\d',val):
				MsgRe.resultAll = re.findall(r'\d:\d \([0-9a-zA-Z;: ]*\)',val) #так ищем итоговые данные о матче
				#print('Обнаружены итоговые данные о раундах!!!!!')
				pass
			else:
				#print('Данных о раунде не обнаружено')
				pass
		#============================================
		#	Отсюда пишем логику сигнализирующего бота
		#============================================
		tsignal = '🎯Ловим ставку в матче\n🎮' + MsgRe.players[0] + ' - ' + MsgRe.players[1] + '\n📈Ставка: '
		#t = send(MsgRe.time,MsgRe.players,MsgRe.qP,MsgRe.qF,MsgRe.qT,MsgRe.resinRound,MsgRe.resultAll)
		if MsgRe.qT:
			#print(len(MsgRe.resinRound))
			if len(MsgRe.resinRound)==1:#что делаем если в сообщении 1 раунд
				#==========================================================================================================
				#information('g','В измененном сообщении 1 раунд')
				if centerLineCheck(MsgRe.qT,MsgRe.resinRound):
					information('g','Отправлено предупреждение в канал '+MsgRe.players[0] + ' - ' + MsgRe.players[1])
					t = 'Внимание на бой\n'+'#'+MsgRe.players[0]+' - #'+MsgRe.players[1]+'\n#'+MsgRe.players[2]
					#print(t)
					message = await client.send_message(exportChannel, str(t))
					appendToTxt(message.id,MsgRe.time)#если обнаружен пробив в первом раунде записываем в файл!!
				else:
					information('r','В новом матче сигнала не будет')
				#==========================================================================================================
			elif len(MsgRe.resinRound)==2:#что делаем если в сообщении 2 раунда
				#==========================================================================================================
				#information('r','В сообщении обнаружено 2 раунда')
				idMsg = returnIdMsgTxt(MsgRe.time)
				if idMsg:
					#тут проверка куда пробило в первом раунде и в зависимости от этого подтверждение сигнала или удаление
					#print(centerLineCheck(MsgRe.qT,MsgRe.resinRound))
					if centerLineCheck(MsgRe.qT,MsgRe.resinRound) == True:
						r1=MsgRe.resinRound[0][0][2]
						r2=MsgRe.resinRound[1][0][2]
						qMin = MsgRe.qT[0]
						qMax = MsgRe.qT[2]
						if float(r1)+k < float(qMin) and float(r2)+k < float(qMin):
							information('g','Подтвержден сигнал ТБ: '+MsgRe.players[0] + ' - ' + MsgRe.players[1])
							tsignal=tsignal+' 3 раунд/ТБ/'+MsgRe.qT[1]
							await client.edit_message(exportChannel, int(idMsg), str(tsignal))
						if float(r1)-k > float(qMax) and float(r2)-k > float(qMax):
							information('g','Подтвержден сигнал ТМ: '+MsgRe.players[0] + ' - ' + MsgRe.players[1])
							tsignal=tsignal+' 3 раунд/ТМ/'+MsgRe.qT[1]
							await client.edit_message(exportChannel, int(idMsg), str(tsignal))
					else:
						information('r','Сигнал не подтвержден: '+MsgRe.players[0] + ' - ' + MsgRe.players[1])
						delStrTxt(MsgRe.time)#Если сигнал во втором раунде не подтвердился то удаляем запись в файле
						await client.delete_messages(exportChannel, int(idMsg))
						pass#тут сигнал не подтвердился во втором раунде удаляем его из файла и сообщение
				else:
					pass
					#information('g','Изменено сообщение которого нет в списке(пробива в нем нет)')
				#==========================================================================================================
			elif len(MsgRe.resinRound)>=3:#что делаем если в сообщении 3 раунда
				#==========================================================================================================
				idMsg = returnIdMsgTxt(MsgRe.time)
				#information('r','В сообщении обнаружено 3 или более раундов')
				if idMsg:#Сообщение в котором 3 или более раундов найдено и с ним можно работать
					r1=MsgRe.resinRound[0][0][2]
					r2=MsgRe.resinRound[1][0][2]
					r3=MsgRe.resinRound[2][0][2]
					qMin = MsgRe.qT[0]
					qMid = MsgRe.qT[0]
					qMax = MsgRe.qT[2]
					
					if len(MsgRe.resinRound)>=3:#ыполняется когда раундов больше 3 и заход события не обнаружен
						#print('Начинаем проверку в каждом раунде дальше второго')
						count = 0
						while count+2 <len(MsgRe.resinRound):
							#print('Проверяем'+str(count+3)+'раунд')
							if float(r1)+k < float(qMin) and float(r2)+k < float(qMin) and float(MsgRe.resinRound[count+2][0][2]) > float(MsgRe.qT[1]):
								#проверяем в каждом раунде что тотал !!!!!_БОЛЬШЕ_!!!!! среднего
								#print('Прогноз на тотал больше сбылся в '+ str(count+3) +' раунде')
								information('g','Прогноз ТБ сбылся в: '+ str(count+3)+' раунде'+MsgRe.players[0] + ' - ' + MsgRe.players[1])
								tsignal=tsignal+' 3 раунд/ТБ/'+MsgRe.qT[1]+'\n✅В ' + str(count+3) + ' раунде' 
								await client.edit_message(exportChannel, int(idMsg), str(tsignal))
								delStrTxt(MsgRe.time)
								break
							else:
								pass
								#print('Прогноз на тотал больше НЕ сбылся в '+ str(count+3) +' раунде')
							if float(r1)-k > float(qMax) and float(r2)-k > float(qMax) and float(MsgRe.resinRound[count+2][0][2]) < float(MsgRe.qT[1]):
								#проверяем в каждом раунде что тотал !!!!!_МЕНЬШЕ_!!!!! среднего
								#print('Прогноз на тотал меньше сбылся в '+ str(count+3) +' раунде')
								information('g','Прогноз ТМ сбылся в: '+ str(count+3)+' раунде'+MsgRe.players[0] + ' - ' + MsgRe.players[1])
								tsignal=tsignal+' 3 раунд/ТМ/'+MsgRe.qT[1]+'\n✅В ' + str(count+3) + ' раунде' 
								await client.edit_message(exportChannel, int(idMsg), str(tsignal))
								delStrTxt(MsgRe.time)
								break
							else:
								pass
								#print('Прогноз на тотал меньше НЕ сбылся в '+ str(count+3) +' раунде')
							if int(MsgRe.resultAll[0][0]) == 5 or int(MsgRe.resultAll[0][2]) == 5:
								information('r','Прогноз НЕ сбылся ни в одном из раундов'+MsgRe.players[0] + ' - ' + MsgRe.players[1])
								tsignal=tsignal+' 3 раунд/ТБ/'+MsgRe.qT[1]+'\n❌\n'
								await client.edit_message(exportChannel, int(idMsg), str(tsignal))
								delStrTxt(MsgRe.time)
								break
							
							count = count + 1

				else:
					pass
				#==========================================================================================================
			MsgRe.resinRound = []
			result = []
		else:
			information('r','В сообщении не найдены коэффициенты тоталов')
			print(MsgRe.resinRound)
		# MsgRe.resinRound = []
		# MsgRe.resultAll = []
		# lastmsg = open(filemsg,'r')
		# tempmsg = []
		# for line in lastmsg:
		# 	#print(get_info_fromtxt(line))
		# 	tempmsg.append(get_info_fromtxt(line))#добавление строки к массиву с последними сообщениями парсинг txt файла в массив
		# lastmsg.close()
		# #print(tempmsg,MsgRe.time)
		# idMsg = getIdOfMsg(tempmsg,MsgRe.time)
		# #print('Отредактировать сообщение с ID'+str(idMsg))
		# await client.edit_message(exportChannel, int(idMsg), str(t))
		# information('g','Отредактировано сообщение в канале с ID-'+str(idMsg))
		MsgRe.resinRound = []
		result = []
	else:
		information('r','Отредактировано рекламное сообщение')
	#print('Message', event.id, 'changed at', event.date)
	#print(event.message.to_dict()['message'])
client.run_until_disconnected()